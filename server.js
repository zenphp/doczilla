import express from 'express';
import { exec } from 'child_process';

const app = express();
const PORT = 3333;

app.use(express.json());

app.post('/webhook', (req, res) => {
  exec('./bin/webhook.sh', (error, stdout, stderr) => {
    if (error) {
      console.error(`Execution error: ${error}`);
      return res.status(500).send(`Error: ${error.message}`);
    }
    res.status(200).send(`Output: ${stdout}`);
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
