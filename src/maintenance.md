---
layout: maintenance
---

<script setup>
  import Maintenance from '@/components/Maintenance/Maintenance.vue'
</script>

<Maintenance />
